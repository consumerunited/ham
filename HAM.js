/*
  HAM.js - Harmony Agent Monitor
  Phantom.js script that logs into harmony and retrieves agent logins
*/

var steps=[];
var testindex = 0;
var loadInProgress = false;//This is set to true when a page is still loading
var users = [];
var fs = require('fs');

/*********SETTINGS*********************/
var webPage = require('webpage');
var page = webPage.create();
var system = require('system'), address;

page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;


/*********SETTINGS END*****************/

//console.log('All settings loaded, start with execution');
page.onConsoleMessage = function(msg) {
  console.log(msg);
};

steps = [
//Step 1.5 - Logout if logged in already to prevent too many sessions
  // function() {
  //   console.log("logout to end previous session")        
  //   page.open("https://acmhcuiiisr1mpls.noblehosted.com/Harmony/Utilities/Sessions");
  //   page.evaluate(function() {
  //     if(document.title == "Sessions") {
  //       https://acmhcuiiisr1mpls.noblehosted.com/Harmony/Common/Web/UI/Handlers/View.aspx?clientID=ctl00_cphContent_ctlSessionList&src=~/Management/Utilities/Sessions/List.ascx&cacheBust=1228463782376
  //       console.log("logging out of previous session");
  //       __doPostBack("ctl00$tbiLogoutButton",'');
  //     }
  //   });
  // },

  //Step 1 - Open home page
  function(){
    console.log("open home page")
    page.open("https://acmhcuiiisr1mpls.noblehosted.com/Harmony/Login", function(status){
      console.log('Stripped down page text:\n' + page.plainText);
    });
  },
  
  //Step 2 - Populate and submit the login form
  function(){
    console.log('Step 2 - Populate and submit the login form');
    page.evaluate(function(){
      document.getElementById("ctl00_cphMain_loginDialog_ctl02_ctl07_txtUsername").value="allen.mackenzie";
      document.getElementById("ctl00_cphMain_loginDialog_ctl02_ctl07_txtPassword").value="Welcome20!5";
      __doPostBack('ctl00$cphMain$loginDialog$ctl02$ctl16$lnkLogin','');
    });
  },

  //Step 3
  function(){
    console.log('navigate to wallboard');
    page.open("https://acmhcuiiisr1mpls.noblehosted.com/Harmony/Wallboard/Views/Wallboard.aspx", function(status){
      console.log('Stripped down page text:\n' + page.plainText);
      // TODO: check the text here, if not what is expected we didn't get to the wallboard
    });
  },

];


//updated function to loop until error, service restarts if error is found, logs back in.
function scrapeWallboard() {
  
  // Output content of page to stdout after form has been submitted
  var agents = page.evaluate(function(crashme) {

    // if after 6pm then logout and start again tomorrow from cron
    var d = new Date();
    if(d.getHours() > 20) {
     // run logout script
     console.log("logging out");
      __doPostBack('ctl00$lnkLogout','');
      return "exit";
    }

    //Getting table from wallboard
    var t = document.getElementsByClassName('yui-dt-data')[0];

    var agentsLoggedIn = null;

    // if there are no t.rows we are not on the wallboard
    if (t === undefined || t.rows == null ){
      console.log('t.rows not defined')
      return null;
    } else {
      agentsLoggedIn = t.rows.length;
    }
  
    //echoing new value of agentsLoggedIn
    //console.log('There are ' + agentsLoggedIn + '  Agents logged in at this time')

    //Printing out first line of raw row content
    //console.log('The first row textContent of T is '+ t.rows[0].textContent);
    //New Array
    agentList = [];
    JSONcounter = 1;
    
    //Working for loop, goes through all agents records information
    for ( var index=0; index < agentsLoggedIn ; index++){
      //console.log('')
      var agentName = t.rows[index].getElementsByClassName("yui-dt0-col-FNME");
      var dialerCode = t.rows[index].getElementsByClassName("yui-dt0-col-NAME");
      var status = t.rows[index].getElementsByClassName("yui-dt0-col-STAT");
      var group = t.rows[index].getElementsByClassName("yui-dt0-col-GRP");
      var time = t.rows[index].getElementsByClassName("yui-dt0-col-TMSC");
      var campaign = t.rows[index].getElementsByClassName("yui-dt0-col-APPL");
      
      agentList[index] = {DialerCode:dialerCode[0].textContent, AgentName:agentName[0].textContent, Status:status[0].textContent, Group:group[0].textContent, Time:time[0].textContent, Campaign:campaign[0].textContent};
      console.log(JSONcounter++ + ' of ' + t.rows.length + ' added to JSON String');      
    }

    return agentList;

  });
  
  console.log(JSON.stringify(agents));

  if(agents == "exit") {
    phantom.exit(0);
  } else if(agents) {
    alert = false;
    var agentsPage = webPage.create();
    var settings = {
      operation: "POST",
      encoding: "utf8",
      headers: { "Content-Type": "application/json" },
      data: JSON.stringify(agents)
    }

    agentsPage.open('http://localhost/agents', settings, function(status) {
      if (status !== 'success') {
            console.log('Unable to post!');
            console.log(status);
            agentsPage.close();
        } else {
            console.log(agentsPage.content);
            var path = "/var/log/ham.log";
            fs.write(path, JSON.stringify(agents), 'w');
            agentsPage.close();
        }
    });    

  } else {
    // t.rows is not defined and we are not on the wallboard, try to re-login
    page.clearCookies();
    testindex = 0;
  }
}  


//Execute steps one by one
//interval = setInterval(executeRequestsStepByStep,15000);

function executeRequestsStepByStep(){
  
  if (typeof steps[testindex] != "function") {
      // scrape wallboard
      console.log("scrape wallboard");
        scrapeWallboard();
  }

  if (loadInProgress == false && typeof steps[testindex] == "function") {
        //console.log("step " + (testindex + 1));
        steps[testindex]();
        testindex++;
  }

  // read a delay file here to change the delay amount, this file is updated by an external page in the lead-buy control panel
  var delayAmount;

  try {
    var delayFile = fs.open('/var/www/ham/delay.txt', 'r');
    delayAmount = delayFile.readLine();
    delayFile.close();
  } catch (err) {
    delayAmount = 15000;
  }
  
  console.log("delayAmount:"+delayAmount);
  setTimeout( function() {
      executeRequestsStepByStep()
  }, delayAmount);
}

executeRequestsStepByStep();

function evaluate(page, func) {
  var args = [].slice.call(arguments, 2);
  var fn = "function() { return (" + func.toString() + ").apply(this, " + JSON.stringify(args) + ");}";
  return page.evaluate(fn);
}

/**
 * These listeners are very important in order to phantom work properly. 
   Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
 page.onLoadStarted = function() {
  loadInProgress = true;
  //console.log('Loading started');
};
page.onLoadFinished = function() {
  loadInProgress = false;
  //console.log('Loading finished');

};
page.onConsoleMessage = function(msg) {
  console.log(msg);
};
